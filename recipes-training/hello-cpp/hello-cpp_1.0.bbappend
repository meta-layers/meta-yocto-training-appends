# looks like with this hack we can work like with externalsrc
# just modify the sources and they will be picked up by BitBake
#
# top level path of local git repos
LOCAL_SOURCES_PATH = "workdir/sources/meta-yocto-training-sources"
# point to the local git repo
SRC_URI = "file:///${LOCAL_SOURCES_PATH}/${BPN};protocol=file;branch=master"
# adjust S accordingly
S = "${WORKDIR}/${LOCAL_SOURCES_PATH}/${BPN}"
