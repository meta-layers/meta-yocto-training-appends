# top level path of local git repos
LOCAL_SOURCES_PATH = "workdir/sources/meta-yocto-training-sources"
# adjust S accordingly
S = "${WORKDIR}/${LOCAL_SOURCES_PATH}/${BPN}-cpp"

# so we can hack the sources:
inherit externalsrc
EXTERNALSRC = "/${LOCAL_SOURCES_PATH}/${BPN}-cpp"
