# top level path of local git repos
LOCAL_SOURCES_PATH = "workdir/sources/meta-yocto-training-sources"
# point to the local git repo
SRC_URI = "file:///${LOCAL_SOURCES_PATH}/${BPN};protocol=file;branch=2.12"
# adjust S accordingly
S = "${WORKDIR}/${LOCAL_SOURCES_PATH}/${BPN}"
