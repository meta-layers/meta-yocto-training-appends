# top level path of local git repos
LOCAL_SOURCES_PATH = "workdir/sources/meta-yocto-training-sources"
# point to the local git repo
SRC_URI = "file:///${LOCAL_SOURCES_PATH}/${BPN};protocol=file;branch=master"
# adjust S accordingly
S = "${WORKDIR}/${LOCAL_SOURCES_PATH}/${BPN}"

# tricky: we apply the patches from the recipe here - we don't have patches in the bbappend dir
SRC_URI += "file://0001-don-t-hardcode-the-tools-and-flags.patch"
SRC_URI += "file://0002-QA-Issue-File-usr-bin-sl-in-package-sl-doesn-t-have-.patch"
