# top level path of local git repos
#LOCAL_SOURCES_PATH = "workdir/sources/meta-yocto-training-sources"
# point to the local git repo
#SRC_URI = "file:///${LOCAL_SOURCES_PATH}/${BPN};protocol=file;branch=master"
# adjust S accordingly
#S = "${WORKDIR}/${LOCAL_SOURCES_PATH}/${BPN}"

# top level path of local git repos
LOCAL_SOURCES_PATH = "workdir/sources/meta-yocto-training-sources"
# adjust S accordingly
S = "${WORKDIR}/${LOCAL_SOURCES_PATH}/hellocppcmake"

# so we can hack the sources:
inherit externalsrc
EXTERNALSRC = "/${LOCAL_SOURCES_PATH}/hellocppcmake"
