#!/bin/bash
source config.sh
rm -rf /tmp/untracked
mkdir /tmp/untracked
cd ../
git ls-files --others | xargs -I {} rsync ./{} /tmp/untracked -avp --relative
set -x
cd /tmp/untracked
# remove the toolchains from tarball
find . -name "resy-*\-toolchain\-${YOCTO_VERSION_NUMBER}*" -delete
find . -name "resy-*\-toolchain-ext-${YOCTO_VERSION_NUMBER}*" -delete
find . -name "*\-nativesdk\-standalone\-${YOCTO_VERSION_NUMBER}.sh" -delete
find . -type d -name "kprobe_test_session-*" -exec rm -rf {} +
find . -type d -name "kernel_ust_test_session-*" -exec rm -rf {} +
find . -type d -name "test_session-*" -exec rm -rf {} +
# tree after we removed
tree -C /tmp/untracked
cd /tmp/untracked
tar czvf ../untracked.tar.gz .
scp ../untracked.tar.gz rber@${LAB}:/tmp
